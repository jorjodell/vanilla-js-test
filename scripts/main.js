"use strict";

window.onload = function () {
  request({pathname: 'vehicles.json'}, function (err, res) {
    if(err) throw err;
    try {
      var store = new Store();
      var data = JSON.parse(res);
      store.setData(data);
      Object.keys(store).forEach(function (key) {
        if(store[key].length) {
          drawRows(store[key], store[key][0].table);
        }
      });
    } catch (err) {
      console.error(err);
    }
  });
};


function request(options, callback) {
  var req = new XMLHttpRequest();
  req.open(options.method || "GET", options.pathname, true);
  req.onload = function() {
    if (req.status < 400) {
      callback(null, req.responseText);
    } else {
      callback(new Error("Request failed: " + req.statusText));
    }
  };
  req.onerror = function() {
    callback(new Error("Network error"));
  };
  req.send(options.body || null);
}


function toRow(obj) {
  var row = document.createElement('tr');

  Object.keys(obj).forEach(function(key) {
    var cell = document.createElement('td');
    cell.innerText = obj[key];
    row.appendChild(cell);
  });

  return row;
}


function drawRows(arr, container) {
  var fragment = document.createDocumentFragment();
  for(var i = 0, arrLen = arr.length; i < arrLen; i++) {
    fragment.appendChild(toRow(arr[i]));
  }
  container.appendChild(fragment);
}
