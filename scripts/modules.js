"use strict";

function Vehicle(title, speed, capacity) {
  this.title = title || null;
  this.speed = speed || null;
  this.capacity = capacity || null;
}

function Automobile(data) {
  Vehicle.call(this, data.name, data.speed, data.capacity);
  this.body = data.body || null;
}

Automobile.prototype = Object.create(Vehicle.prototype);
Automobile.prototype.table = document.getElementById('tbl_auto');


function Boat(data) {
  Vehicle.call(this, data.name, data.speed, data.capacity);
  this.maxPower = data.maxpower || null;
}

Boat.prototype = Object.create(Vehicle.prototype);
Boat.prototype.table = document.getElementById('tbl_boat');

function Airplane(data) {
  Vehicle.call(this, data.name, data.speed, data.capacity);
  this.wingspan = data.wingspan || null;
}

Airplane.prototype = Object.create(Vehicle.prototype);
Airplane.prototype.table = document.getElementById('tbl_air');


function VehicleFactory(vehicle) {
  switch (vehicle.type) {
    case 'auto': return new Automobile(vehicle);
    case 'boat': return new Boat(vehicle);
    case 'airplane': return new Airplane(vehicle);
  }
}

function Store() {
  if (Store.instance instanceof Store) {
    return Store.instance;
  }
  this.autos = [];
  this.boats = [];
  this.airplanes = [];
  Store.instance = this;
}

Store.prototype.setData = function (dataArray) {
  dataArray.forEach(function (item) {
    this[item.type + 's'].push(VehicleFactory(item));
  }.bind(this));
  Store.instance = this;
};
